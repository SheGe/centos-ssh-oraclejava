FROM jdeathe/centos-ssh:2.3.0

EXPOSE 22

# -----------------------------------------------------------------------------
# Environment variables
# -----------------------------------------------------------------------------
ENV JAVA_VERSION_MAJOR=8 \
    JAVA_VERSION_MINOR=152 \
    JAVA_VERSION_BUILD=16 \
    JAVA_URL_HASH="aa0333dd3019491ca4f6ddbe78cdb6d0" \
    JAVA_JDK_SHA256="b95c69b10e41d0f91e1ae6ef51086025535a43235858326a5a8fd9c5693ecc28"

# -----------------------------------------------------------------------------
# Install Oracle java
# -----------------------------------------------------------------------------
RUN cd $HOME \
    && curl -L -O -H "Cookie: oraclelicense=accept-securebackup-cookie" \
            -k "http://download.oracle.com/otn-pub/java/jdk/${JAVA_VERSION_MAJOR}u${JAVA_VERSION_MINOR}-b${JAVA_VERSION_BUILD}/${JAVA_URL_HASH}/jdk-${JAVA_VERSION_MAJOR}u${JAVA_VERSION_MINOR}-linux-x64.rpm" \
    && sha256sum jdk-${JAVA_VERSION_MAJOR}u${JAVA_VERSION_MINOR}-linux-x64.rpm | grep $JAVA_JDK_SHA256 \
    && yum install -y jdk-${JAVA_VERSION_MAJOR}u${JAVA_VERSION_MINOR}-linux-x64.rpm \
    && rm -f $HOME/jdk-${JAVA_VERSION_MAJOR}u${JAVA_VERSION_MINOR}-linux-x64.rpm \
    && yum clean all

RUN alternatives --install /usr/bin/java jar /usr/java/latest/bin/java 200000 \
    && alternatives --install /usr/bin/javaws javaws /usr/java/latest/bin/javaws 200000 \
    && alternatives --install /usr/bin/javac javac /usr/java/latest/bin/javac 200000

# -----------------------------------------------------------------------------
# Environment variables
# -----------------------------------------------------------------------------
ENV JAVA_HOME="/usr/java/latest"
ENV PATH="$PATH:$JAVA_HOME/bin"

# -----------------------------------------------------------------------------
# Copy files into place
# -----------------------------------------------------------------------------
ADD src/etc/services-config/supervisor/supervisord.conf \
        /etc/services-config/supervisor/supervisord.conf

RUN ln -sf \
        /etc/services-config/supervisor/supervisord.conf \
        /etc/supervisord.conf

ADD .version centos-ssh-oraclejava.version