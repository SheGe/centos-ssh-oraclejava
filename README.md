# CentOS (SSH) - Oracle Java

## Description

The CentOS (SSH) docker image with installed and preconfigured Oracle Java.
Based on: [jdeathe/centos-ssh](https://store.docker.com/community/images/jdeathe/centos-ssh).

## Features

* Oracle Java installed and configured
* Supervisord controller configured to enable a control through `supervisorctl` command

## Tags and respective `Dockerfile` links

* centos-ssh-oraclejava, 1.0.3 ([Dockerfile](/Dockerfile))

## Agreements

The source code of this container is on the [MIT License](./LICENSE), but usage of this container requires an agreement for [Oracle License](http://www.oracle.com/technetwork/java/javase/terms/license/index.html), aspecially:

> Use of the Commercial Features for any commercial or production purpose requires a separate license from Oracle. "Commercial Features" means those features identified Table 1-1 (Commercial Features In Java SE Product Editions) of the Java SE documentation accessible at http://www.oracle.com/technetwork/java/javase/documentation/index.html
