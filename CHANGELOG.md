# CHANGELOG

## v1.0.4 - 2017-12-07

* The base image has been upgraded to [jdeathe/centos-ssh:2.3.0](https://store.docker.com/community/images/jdeathe/centos-ssh)
* Oracle Java updated to `8u152b16` version

## v1.0.3

* Supervisord controller configured to enable a control through `supervisorctl` command

## v1.0.2

* The base image has been upgraded to [jdeathe/centos-ssh:2.2.4](https://store.docker.com/community/images/jdeathe/centos-ssh)
* Cake bootstraper files updated to support cake protocol change from [HTTP to HTTPS](https://cakebuild.net/blog/2017/08/http-to-https)

## v1.0.1

* Oracle Java updated to `8u144b01` version
* The name of version file in container changed to `centos-ssh-oraclejava.version`

## v1.0.0

* Created a docker image with Oracle JAVA installed (version `8u141b15`)
* The [jdeathe/centos-ssh:2.2.3](https://store.docker.com/community/images/jdeathe/centos-ssh) was choosen as base OS image (CentOS 7) to add SSH featureas and option to run multiple services in container by [supervisord](http://supervisord.org/).