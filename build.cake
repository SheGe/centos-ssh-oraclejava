#addin "Cake.Docker"
#addin "Cake.FileHelpers"

var target = Argument("target", "Default");
var noCache = Argument("nocache", false);
var imageName = Argument("imageName", "shege/centos-ssh-oraclejava");

var dockerRegistry = Argument("dockerRegistry", "registry.gitlab.com");
var dockerUser = Argument<string>("dockerUser", null);
var dockerPassword = Argument<string>("dockerPassword", null);

var version = FileReadText(".version");

Task("Build")
    .Does(() =>
{
    var settings = new DockerBuildSettings
    {
        ForceRm = true,
        File = File("Dockerfile"),
        Tag = new []
        {
            GetImageName("latest", dockerRegistry),
            GetImageName(version, dockerRegistry),
        },
        NoCache = noCache,
    };

    DockerBuild(settings, ".");
});

Task("Publish")
    .IsDependentOn("Build")
    .Does(() =>
{
    LoginToDockerRegistry(dockerRegistry);

    DockerPush(GetImageName("latest", dockerRegistry));
    DockerPush(GetImageName(version, dockerRegistry));
});

Task("Default")
    .IsDependentOn("Build");

RunTarget(target);

private string GetImageName(string tagName, string registryName)
{
    return string.Format("{0}/{1}:{2}", registryName, imageName, tagName);
}

private void LoginToDockerRegistry(string registryName)
{
    var parameters = "login -u {0} -p {1} {2}";

    StartProcess("docker", string.Format(parameters, dockerUser, dockerPassword, registryName));
}